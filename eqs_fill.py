'''
    File:   eqs_fill.py
    Author: dzhao@uw.edu
    Date:   9/23/2018
    Desc:   Fill and rename variables for equations 
'''

# Variable Mapping
# X's                                   Symbols in General Equations
# ====================================================================
# x[0..n^2):                            R_11, R_12, ..., R_nn 
# x[n^2..n^2+(n-1)*n^2):                Ua_111, Ua_112, ..., Ua_nn(n-1)
# x[n^2+(n-1)*n^2..n^2+2*(n-1)*n^2):    Ub_111, Ub_112, ..., Ub_nn(n-1)
# x[n^2+2*(n-1)*n^2):                   U (or U_ij to pad up)

import numpy as np
import re
import math
import sys
from eqs_gen import eqs_gen
from eqs_gen_z2a import eqs_gen_z2a

# Assign x[n^3-(n-1)*n^2..2*n^3] to all Ub's 
def rename_ub (n, eqs):

    for idx in range(len(eqs)):
        tmp = re.findall(r'Ub\[(\d+)\]\[(\d+)\]\[(\d+)\]', eqs[idx])
        if tmp:
            for found in tmp:
                i = int(found[0])
                j = int(found[1])
                k = int(found[2])
                x = 'x['+str(n**2 + (n-1)*n**2 + (i-1)*n*(n-1) + (j-1)*(n-1) + (k-1))+']'
                eqs[idx] = re.sub(r'Ub\['+str(i)+'\]\['+str(j)+'\]\['+str(k)+'\]', x, eqs[idx]) 

# Assign x[n^2..n^2+(n-1)*n^2] to all Ua's 
def rename_ua (n, eqs):

    for idx in range(len(eqs)):
        tmp = re.findall(r'Ua\[(\d+)\]\[(\d+)\]\[(\d+)\]', eqs[idx])
        if tmp:
            for found in tmp:
                i = int(found[0])
                j = int(found[1])
                k = int(found[2])
                x = 'x['+str(n**2 + (i-1)*n*(n-1) + (j-1)*(n-1) + (k-1))+']'
                eqs[idx] = re.sub(r'Ua\['+str(i)+'\]\['+str(j)+'\]\['+str(k)+'\]', x, eqs[idx]) 

# Assign x[n**2+(n-1)*2*n**2] to all U's, if U_ij are identical 
# Assign x[2*n^3-n^2..2*n^3] to all U's, if U_ij are different 
def rename_u (n, eqs):

    # If we assume all U_ij are equal
    for idx in range(len(eqs)):
        # For now, let's assume the input voltage is 0.01v
        eqs[idx] = re.sub(r'U\[\d+\]\[\d+\]', r'.01', eqs[idx]) 
        #eqs[idx] = re.sub(r'U\[\d+\]\[\d+\]', r'x['+str(n**2+(n-1)*2*n**2)+']', eqs[idx]) 
''' # If we want to have different U_ij
    for idx in range(len(eqs)):
        tmp = re.findall(r'U\[(\d+)\]\[(\d+)\]', eqs[idx])
        if tmp:
            for found in tmp:
                i = int(found[0])
                j = int(found[1])
                x = 'x['+str(n**2 + (i-1)*n + (j-1))+']'
                eqs[idx] = re.sub(r'U\['+str(i)+'\]\['+str(j)+'\]', x, eqs[idx]) 
'''

# Assign first x[1..n^2] to all R's, or A's 
def rename_r (n, eqs):

    for idx in range(len(eqs)):
        tmp = re.findall(r'[RA]\[(\d+)\]\[(\d+)\]', eqs[idx])
        if tmp:
            for found in tmp:
                i = int(found[0])
                j = int(found[1])
                x = 'x['+str((i-1)*n + (j-1))+']'
                eqs[idx] = re.sub(r'[RA]\['+str(i)+'\]\['+str(j)+'\]', x, eqs[idx]) 

# Assign Z values to the equations
def assign_z (z_list, eqs):

    n = int(math.sqrt(len(z_list)))
    z = np.reshape(z_list, (n, n))

    for idx in range(len(eqs)):

        # Warning: it will not work by the following, because re is complied before runtime
        #new_eq = re.sub(r'Z\[(\d+)\]\[(\d+)\]', str(Z[int('\g<1>')-1][int('\g<2>')-1]), eq)

        # The following works, although in a more native way: 
        tmp = re.findall(r'Z\[(\d+)\]\[(\d+)\]', eqs[idx])
        if tmp:
            for found in tmp:
                i = int(found[0])
                j = int(found[1])
                z_val = z[i-1][j-1]
                eqs[idx] = re.sub(r'Z\['+str(i)+'\]\['+str(j)+'\]', str(z_val), eqs[idx]) 

    return eqs

# Instantiate the equations
def eqs_fill(z_list, eqs):
    assign_z(z_list, eqs)
    n = int(math.sqrt(len(z_list)))
    rename_r(n, eqs)
    rename_u(n, eqs)
    rename_ua(n, eqs)
    rename_ub(n, eqs)

# Generate the function and persist it to a Python file
def eqs_func(eqs):
    f = open("eqs_func.py", "w")
    f.write("def func(x):\n")
    f.write("\treturn [")
    for idx in range(len(eqs)):
        f.write("\n\t\t" + eqs[idx])
        if idx < len(eqs) - 1:
            f.write(",")
    f.write("\n\t]\n")
    f.close()

# For unit test only
if __name__ == '__main__':

    if len(sys.argv) < 3:
        print "Please provide input N and Zs (" + sys.argv[0] + "  <N> <filename: Zs line-by-line>)"
        exit(0)

    n = int(sys.argv[1])
    with open(sys.argv[2]) as f:
        content = f.readlines()
    content = [x.strip() for x in content]
    #if n**2 > len(content):
        #print "Warning: " + sys.argv[0] + ": Mismatched N^2 and Z's", "N^2 =", n**2, "len(content) =", len(content), ", will use repeated values from the measurement."
    z_list = []
    for i in range(n**2):
        z_list += [float(content[i % len(content)])]

    eqs = eqs_gen(n)
    eqs_fill(z_list, eqs)
    eqs_func(eqs)
