#!/bin/bash

# File:     run.sh
# Author:   dzhao@uw.edu
# Date:     9/23/2018
# Desc:     Wrapper of the non-linear equations' solver
# Use:      ./run.sh [FNAME=measurement.file] [N=int(sqrt(`wc -l measurement.file`))]

# Required input: FNAME (measurement file name)
FNAME=measurement.file

if [ $# -gt 0 ];then
    args=("$@")
    FNAME=${args[0]}
fi
    
############################
# Don't change the following
############################

# Calculate N
LINE_CNT=`wc -l $FNAME`
N_SQR=$(echo $LINE_CNT | cut -d' ' -f 1)
N_FLOAT=`echo "sacle=0; sqrt ($N_SQR)" | bc -l`
N=${N_FLOAT%.*}

if [ $# -gt 1 ];then
    args=("$@")
    N=${args[1]}
fi

# Generate the system of nonlinear functions
SECONDS=0
python eqs_fill.py $N $FNAME
duration=$SECONDS
fsize=`ls -la eqs_func.py | cut -d" " -f5`

#echo $N $duration $fsize
echo "N: $N; Generation time: $duration seconds; Equation size: $fsize"
# Un-Comment the following if you only want to generate the equations
# exit

# Find the solution
SECONDS=0
python eqs_sol.py $N $FNAME
duration=$SECONDS
echo "N: $N; Solution time: $(($duration / 60)) minutes and $(($duration % 60)) seconds."
date

# write the log
echo "$N, $duration" >> log.file
