#!/bin/bash

# File:     run_batch.sh
# Author:   dzhao@uw.edu
# Date:     9/23/2018
# Desc:     Batch processing of run.sh
# Use:      ./run_batch <start> <end>

if [ $# -lt 2 ];
then
    echo "Please provide start and end indices"
    exit 0
fi

args=("$@")
start=${args[0]}
end=${args[1]}
step=1
if [ $# -gt 2 ];
then
    step=${args[2]}
fi

rm -f log.file
for (( i=$start; i<=$end; i=$i+step ))
do
    ./run.sh measurement.file $i 
done

